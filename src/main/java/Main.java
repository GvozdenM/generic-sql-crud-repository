import ModelsPuigPedro.*;
import Repositories.AbstractJDBCRepository;
import Table.Entity.*;
import org.jetbrains.annotations.NotNull;

import javax.xml.transform.Result;
import java.sql.*;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class Main {
    private static final String PUIG_CONNECTION_URL =
            "jdbc:mysql://puigpedros.salle.url.edu/?user=" + Settings.USER + "&password=" + Settings.PASSWORD + "&serverTimezone=UTC";

    private static final String LOCAL_CONNECTION_URL =
            "jdbc:mysql://localhost:3306/F1Local?user=root&password=password&allowPublicKeyRetrieval=true&useUnicode=true&characterEncoding=utf8&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC";
    public static void main(String[] args) {
        Connection puigConnection = getConnection(PUIG_CONNECTION_URL);
        String puigDb = "F1";

        Connection localConnection = getConnection(LOCAL_CONNECTION_URL);
        String localDb = "F1Local";

        if(null != puigConnection && null != localConnection) {

            AbstractJDBCRepository<Circuits> remoteCircuitRepo = new AbstractJDBCRepository<>(
                    puigConnection,
                    puigDb,
                    TableManager.createTable(Circuits.class),
                    Main::circuitTransform
            );
            AbstractJDBCRepository<ConstructorResults> remoteConstResultRepo = new AbstractJDBCRepository<>(
                            puigConnection,
                            puigDb,
                            TableManager.createTable(ConstructorResults.class),
                            Main::constructorResultsTransform
            );

            AbstractJDBCRepository<Constructors> remoteConstructorRepo = new AbstractJDBCRepository<>(
                            puigConnection,
                            puigDb,
                            TableManager.createTable(Constructors.class),
                            Main::constructorTransform
            );
            AbstractJDBCRepository<ConstructorStandings> remoteConstructorStandingsRepo = new AbstractJDBCRepository<>(
                            puigConnection,
                            puigDb,
                            TableManager.createTable(ConstructorStandings.class),
                            Main::constructorStandingsTransform
            );

            AbstractJDBCRepository<Drivers> remoteDriversRepo = new AbstractJDBCRepository<>(
                            puigConnection,
                            puigDb,
                            TableManager.createTable(Drivers.class),
                            Main::driversTransform
            );

            AbstractJDBCRepository<DriverStandings> remoteDriverStandingsRepo = new AbstractJDBCRepository<>(
                            puigConnection,
                            puigDb,
                            TableManager.createTable(DriverStandings.class),
                            Main::driverStandingsTransform
            );
            AbstractJDBCRepository<Qualifying> remoteQualifyingRepo = new AbstractJDBCRepository<>(
                            puigConnection,
                            puigDb,
                            TableManager.createTable(Qualifying.class),
                            Main::qualifyingTransform
            );
            AbstractJDBCRepository<Races> remoteRacesRepo = new AbstractJDBCRepository<>(
                            puigConnection,
                            puigDb,
                            TableManager.createTable(Races.class),
                            Main::racesTransform
            );
            AbstractJDBCRepository<Results> remoteResultsRepo = new AbstractJDBCRepository<>(
                            puigConnection,
                            puigDb,
                            TableManager.createTable(Results.class),
                            Main::resultsTransform
            );

            AbstractJDBCRepository<Seasons> remoteSeasonsRepo = new AbstractJDBCRepository<>(
                            puigConnection,
                            puigDb,
                            TableManager.createTable(Seasons.class),
                            Main::seasonsTransform
            );


            AbstractJDBCRepository<Status> remoteStatusRepo = new AbstractJDBCRepository<>(
                            puigConnection,
                            puigDb,
                            TableManager.createTable(Status.class),
                            Main::statusTransform
            );

            AbstractJDBCRepository<PitStops> pitStopRepo = new AbstractJDBCRepository<>(
                    puigConnection,
                    puigDb,
                    TableManager.createTable(PitStops.class),
                    Main::pitStopsTransform
            );

            AbstractJDBCRepository<LapTimes> lapTimesRepo = new AbstractJDBCRepository<>(
                    puigConnection,
                    puigDb,
                    TableManager.createTable(LapTimes.class),
                    Main::lapTimesTransform
            );

            AbstractJDBCRepository<Circuits> localCircuitRepo = new AbstractJDBCRepository<>(
                    localConnection,
                    localDb,
                    TableManager.createTable(Circuits.class),
                    Main::circuitTransform
            );
            AbstractJDBCRepository<ConstructorResults> localConstResultRepo = new AbstractJDBCRepository<>(
                    localConnection,
                    localDb,
                    TableManager.createTable(ConstructorResults.class),
                    Main::constructorResultsTransform
            );

            AbstractJDBCRepository<Constructors> localConstructorRepo = new AbstractJDBCRepository<>(
                    localConnection,
                    localDb,
                    TableManager.createTable(Constructors.class),
                    Main::constructorTransform
            );
            AbstractJDBCRepository<ConstructorStandings> localConstructorStandingsRepo = new AbstractJDBCRepository<>(
                    localConnection,
                    localDb,
                    TableManager.createTable(ConstructorStandings.class),
                    Main::constructorStandingsTransform
            );

            AbstractJDBCRepository<Drivers> localDriversRepo = new AbstractJDBCRepository<>(
                    localConnection,
                    localDb,
                    TableManager.createTable(Drivers.class),
                    Main::driversTransform
            );

            AbstractJDBCRepository<DriverStandings> localDriverStandingsRepo = new AbstractJDBCRepository<>(
                    localConnection,
                    localDb,
                    TableManager.createTable(DriverStandings.class),
                    Main::driverStandingsTransform
            );
            AbstractJDBCRepository<Qualifying> localQualifyingRepo = new AbstractJDBCRepository<>(
                    localConnection,
                    localDb,
                    TableManager.createTable(Qualifying.class),
                    Main::qualifyingTransform
            );
            AbstractJDBCRepository<Races> localRacesRepo = new AbstractJDBCRepository<>(
                    localConnection,
                    localDb,
                    TableManager.createTable(Races.class),
                    Main::racesTransform
            );
            AbstractJDBCRepository<Results> localResultsRepo = new AbstractJDBCRepository<>(
                    localConnection,
                    localDb,
                    TableManager.createTable(Results.class),
                    Main::resultsTransform
            );
            AbstractJDBCRepository<Seasons> localSeasonsRepo = new AbstractJDBCRepository<>(
                    localConnection,
                    localDb,
                    TableManager.createTable(Seasons.class),
                    Main::seasonsTransform
            );
            AbstractJDBCRepository<Status> localStatusRepo = new AbstractJDBCRepository<>(
                    localConnection,
                    localDb,
                    TableManager.createTable(Status.class),
                    Main::statusTransform
            );

            AbstractJDBCRepository<PitStops> localPitStopRepo = new AbstractJDBCRepository<>(
                    localConnection,
                    localDb,
                    TableManager.createTable(PitStops.class),
                    Main::pitStopsTransform
            );

            AbstractJDBCRepository<LapTimes> localLapTimesRepo = new AbstractJDBCRepository<>(
                    localConnection,
                    localDb,
                    TableManager.createTable(LapTimes.class),
                    Main::lapTimesTransform
            );

//
//            copyAll(remoteResultsRepo, localResultsRepo);
//            copyAll(remoteRacesRepo, localRacesRepo);
//            copyAll(remoteDriversRepo, localDriversRepo);
//            copyAll(remoteCircuitRepo, localCircuitRepo);
//            copyAll(remoteDriverStandingsRepo, localDriverStandingsRepo);
//            copyAll(remoteQualifyingRepo, localQualifyingRepo);
//            copyAll(remoteConstructorRepo, localConstructorRepo);
//            copyAll(remoteConstResultRepo, localConstResultRepo);
//            copyAll(remoteConstructorStandingsRepo, localConstructorStandingsRepo);
//            copyAll(remoteSeasonsRepo, localSeasonsRepo);
//            copyAll(remoteStatusRepo, localStatusRepo);
  //          copyAll(pitStopRepo, localPitStopRepo);
            copyAll(lapTimesRepo, localLapTimesRepo);
        }
        else {
            System.out.println("Could not connect to one of the databases");
        }

        try {
            puigConnection.close();
            localConnection.close();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    private static <T> void copyAll(AbstractJDBCRepository<T> remoteRepo, AbstractJDBCRepository<T> localRepo) {
        Collection<T> remoteCounterparts = remoteRepo.getAll();

        remoteCounterparts.forEach((o) -> {
            System.out.println(o);
            localRepo.create(o);
        });
    }

    private static Connection getConnection(@NotNull String url) {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            return DriverManager.getConnection(url);
        } catch (SQLException | ClassNotFoundException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    private static Circuits circuitTransform(ResultSet resultSet) {
        try {
            return new Circuits(
                    resultSet.getInt("circuitId"),
                    resultSet.getString("circuitRef"),
                    resultSet.getString("name"),
                    resultSet.getString("location"),
                    resultSet.getString("country"),
                    resultSet.getFloat("lat"),
                    resultSet.getFloat("lng"),
                    resultSet.getInt("alt"),
                    resultSet.getString("url")
            );
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    private static ConstructorResults constructorResultsTransform(ResultSet resultSet) {
        try {
            return new ConstructorResults(
                    resultSet.getInt("constructorResultsId"),
                    resultSet.getInt("raceId"),
                    resultSet.getInt("constructorId"),
                    resultSet.getFloat("points"),
                    resultSet.getString("status")
            );
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    private static Constructors constructorTransform(ResultSet resultSet) {
        try {
            return new Constructors(
                    resultSet.getInt("constructorId"),
                    resultSet.getString("constructorRef"),
                    resultSet.getString("name"),
                    resultSet.getString("nationality"),
                    resultSet.getString("url")
            );
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    private static ConstructorStandings constructorStandingsTransform(ResultSet resultSet) {
        try {
            return new ConstructorStandings(
                    resultSet.getInt("constructorStandingsId"),
                    resultSet.getInt("raceId"),
                    resultSet.getInt("constructorId"),
                    resultSet.getFloat("points"),
                    resultSet.getInt("position"),
                    resultSet.getString("positionText"),
                    resultSet.getInt("wins")
            );
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    private static Drivers driversTransform(ResultSet resultSet) {
        try {
            return new Drivers(
                    resultSet.getInt("driverId"),
                    resultSet.getString("driverRef"),
                    resultSet.getInt("number"),
                    resultSet.getString("code"),
                    resultSet.getString("forename"),
                    resultSet.getString("surname"),
                    resultSet.getDate("dob"),
                    resultSet.getString("nationality"),
                    resultSet.getString("url")
            );
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    private static DriverStandings driverStandingsTransform(ResultSet resultSet) {
        try {
            return new DriverStandings(
                    resultSet.getInt("driverStandingsId"),
                    resultSet.getInt("raceId"),
                    resultSet.getInt("driverId"),
                    resultSet.getFloat("points"),
                    resultSet.getInt("position"),
                    resultSet.getString("positionText"),
                    resultSet.getInt("wins")
            );
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    private static Qualifying qualifyingTransform(ResultSet resultSet) {
        try {
           return new Qualifying(
                   resultSet.getInt("qualifyId"),
                   resultSet.getInt("raceId"),
                   resultSet.getInt("driverId"),
                   resultSet.getInt("constructorId"),
                   resultSet.getInt("number"),
                   resultSet.getInt("position"),
                   resultSet.getString("q1"),
                   resultSet.getString("q2"),
                   resultSet.getString("q3")

           );
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    private static Races racesTransform(ResultSet resultSet) {
        try {
            return new Races(
                    resultSet.getInt("raceId"),
                    resultSet.getInt("year"),
                    resultSet.getInt("round"),
                    resultSet.getInt("circuitId"),
                    resultSet.getString("name"),
                    resultSet.getDate("date"),
                    resultSet.getTime("time"),
                    resultSet.getString("url")
            );
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    private static Results resultsTransform(ResultSet resultSet) {
        try {
            return new Results(
                    resultSet.getInt("resultId"),
                    resultSet.getInt("raceId"),
                    resultSet.getInt("driverId"),
                    resultSet.getInt("constructorId"),
                    resultSet.getInt("number"),
                    resultSet.getInt("grid"),
                    resultSet.getInt("position"),
                    resultSet.getString("positionText"),
                    resultSet.getInt("positionOrder"),
                    resultSet.getFloat("points"),
                    resultSet.getInt("laps"),
                    resultSet.getString("time"),
                    resultSet.getInt("milliseconds"),
                    resultSet.getInt("fastestLap"),
                    resultSet.getInt("rank"),
                    resultSet.getString("fastestLapTime"),
                    resultSet.getString("fastestLapSpeed"),
                    resultSet.getInt("statusId")
            );
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    private static Seasons seasonsTransform(ResultSet set) {
        try {
            return new Seasons(
                    set.getInt("year"),
                    set.getString("url")
            );
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    private static Status statusTransform(ResultSet resultSet) {
        try {
            return new Status(
                    resultSet.getInt("statusId"),
                    resultSet.getString("status")
            );
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    private static LapTimes lapTimesTransform(ResultSet resultSet) {
        try {
            return new LapTimes(
                    resultSet.getInt("raceId"),
                    resultSet.getInt("driverId"),
                    resultSet.getInt("lap"),
                    resultSet.getInt("position"),
                    resultSet.getString("time"),
                    resultSet.getInt("milliseconds")
            );
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }

    private static PitStops pitStopsTransform(ResultSet resultSet) {
        try {
            return new PitStops(
                    resultSet.getInt("raceId"),
                    resultSet.getInt("driverId"),
                    resultSet.getInt("stop"),
                    resultSet.getInt("lap"),
                    resultSet.getTime("time"),
                    resultSet.getString("duration"),
                    resultSet.getInt("milliseconds")
            );
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return null;
    }
}
