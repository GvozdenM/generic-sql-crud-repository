package Annotations;

import java.lang.annotation.*;


/**
 * Annotation for persisted database entities to signify the fields is a
 * foreign key
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface ForeignKey {
    String type() default "";
    String references();
    String referenceField();
}
