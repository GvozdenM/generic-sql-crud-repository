package Annotations;

import java.lang.annotation.*;

/**
 * Annotation for persisted database entities to signify the fields is the
 * primary key.
 */
@Documented
@Target(ElementType.FIELD)
@Retention(RetentionPolicy.RUNTIME)
public @interface PrimaryKey {
}
