package Transformers;

import java.sql.ResultSet;

@FunctionalInterface
public interface ResultSetTransformer<T> {
    /**
     * Transform the given ResultSet to an object of type T
     * @param r The result set to transform
     * @return An instance of T containing the data in the set
     */
    T transform(ResultSet r);
}
