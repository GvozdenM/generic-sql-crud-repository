package ModelsPuigPedro;

import Annotations.EntityAnnotation;
import Annotations.PrimaryKey;

import java.sql.Time;

@EntityAnnotation(tableName = "pitStops")
public class PitStops {
    @PrimaryKey
    private final int raceId;
    private final int driverId;
    private final int stop;
    private final int lap;
    private final Time time;
    private final String duration;
    private final int milliseconds;

    public PitStops(int raceId, int driverId, int stop, int lap, Time time, String duration, int milliseconds) {
        this.raceId = raceId;
        this.driverId = driverId;
        this.stop = stop;
        this.lap = lap;
        this.time = time;
        this.duration = duration;
        this.milliseconds = milliseconds;
    }

    public int getRaceId() {
        return raceId;
    }

    public int getDriverId() {
        return driverId;
    }

    public int getStop() {
        return stop;
    }

    public int getLap() {
        return lap;
    }

    public Time getTime() {
        return time;
    }

    public String getDuration() {
        return duration;
    }

    public int getMilliseconds() {
        return milliseconds;
    }
}
