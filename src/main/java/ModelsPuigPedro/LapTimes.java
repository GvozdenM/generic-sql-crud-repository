package ModelsPuigPedro;

import Annotations.EntityAnnotation;
import Annotations.PrimaryKey;


@EntityAnnotation(tableName = "lapTimes")
public class LapTimes {
    @PrimaryKey
    private final int raceId;
    private final int driverId;
    private final int lap;
    private final int position;
    private final String time;
    private final long milliseconds;

    public LapTimes(int raceId, int driverId, int lap, int position, String time, long milliseconds) {
        this.raceId = raceId;
        this.driverId = driverId;
        this.lap = lap;
        this.position = position;
        this.time = time;
        this.milliseconds = milliseconds;
    }

    public int getRaceId() {
        return raceId;
    }

    public int getDriverId() {
        return driverId;
    }

    public int getLap() {
        return lap;
    }

    public int getPosition() {
        return position;
    }

    public String getTime() {
        return time;
    }

    public long getMilliseconds() {
        return milliseconds;
    }
}
