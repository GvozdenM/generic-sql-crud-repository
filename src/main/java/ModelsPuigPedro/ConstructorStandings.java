package ModelsPuigPedro;


import Annotations.EntityAnnotation;
import Annotations.ForeignKey;
import Annotations.PrimaryKey;

@EntityAnnotation(tableName = "constructorStandings")
public class ConstructorStandings {
    @PrimaryKey
    private final int constructorStandingsId;

    @ForeignKey(references = "races", referenceField = "raceId")
    private final int raceId;

    @ForeignKey(references = "constructors", referenceField = "constructorsId")
    private final int constructorId;
    private final float points;
    private final int position;
    private final String positionText;
    private final int wins;

    public ConstructorStandings(int constructorStandingsId,
                                int raceId,
                                int constructorId,
                                float points,
                                int position,
                                String positionText,
                                int wins) {
        this.constructorStandingsId = constructorStandingsId;
        this.raceId = raceId;
        this.constructorId = constructorId;
        this.points = points;
        this.position = position;
        this.positionText = positionText;
        this.wins = wins;
    }

    public int getConstructorStandingsId() {
        return constructorStandingsId;
    }

    public int getRaceId() {
        return raceId;
    }

    public int getConstructorId() {
        return constructorId;
    }

    public float getPoints() {
        return points;
    }

    public int getPosition() {
        return position;
    }

    public String getPositionText() {
        return positionText;
    }

    public int getWins() {
        return wins;
    }
}
