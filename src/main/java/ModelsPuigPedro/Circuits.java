package ModelsPuigPedro;

import Annotations.EntityAnnotation;
import Annotations.PrimaryKey;

@EntityAnnotation(tableName = "circuits")
public class Circuits {
    @PrimaryKey
    private final int circuitId;
    private final String circuitRef;
    private final String name;
    private final String location;
    private final String country;
    private final float lat;
    private final float lng;
    private final int alt;
    private final String url;

    public Circuits(int circuitId,
                    String circuitRef,
                    String name,
                    String location,
                    String country,
                    float lat,
                    float lng,
                    int alt,
                    String url) {
        this.circuitId = circuitId;
        this.circuitRef = circuitRef;
        this.name = name;
        this.location = location;
        this.country = country;
        this.lat = lat;
        this.lng = lng;
        this.alt = alt;
        this.url = url;
    }

    public int getCircuitId() {
        return circuitId;
    }

    public String getCircuitRef() {
        return circuitRef;
    }

    public String getName() {
        return name;
    }

    public String getLocation() {
        return location;
    }

    public String getCountry() {
        return country;
    }

    public float getLat() {
        return lat;
    }

    public float getLng() {
        return lng;
    }

    public int getAlt() {
        return alt;
    }

    public String getUrl() {
        return url;
    }
}
