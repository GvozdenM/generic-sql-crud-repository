package ModelsPuigPedro;


import Annotations.EntityAnnotation;
import Annotations.ForeignKey;
import Annotations.PrimaryKey;

@EntityAnnotation(tableName = "driverStandings")
public class DriverStandings {
    @PrimaryKey
    private final int driverStandingsId;

    @ForeignKey(references = "races", referenceField = "raceId")
    private final int raceId;

    @ForeignKey(references = "drivers", referenceField = "driverId")
    private final int driverId;
    private final float points;
    private final int position;
    private final String positionText;
    private final int wins;

    public DriverStandings(int driverStandingsId,
                           int raceId,
                           int driverId,
                           float points,
                           int position,
                           String positionText,
                           int wins) {

        this.driverStandingsId = driverStandingsId;
        this.raceId = raceId;
        this.driverId = driverId;
        this.points = points;
        this.position = position;
        this.positionText = positionText;
        this.wins = wins;
    }

    public int getDriverStandingsId() {
        return driverStandingsId;
    }

    public int getRaceId() {
        return raceId;
    }

    public int getDriverId() {
        return driverId;
    }

    public float getPoints() {
        return points;
    }

    public int getPosition() {
        return position;
    }

    public String getPositionText() {
        return positionText;
    }

    public int getWins() {
        return wins;
    }
}
