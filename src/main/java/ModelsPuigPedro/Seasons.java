package ModelsPuigPedro;

import Annotations.EntityAnnotation;
import Annotations.PrimaryKey;

@EntityAnnotation(tableName = "seasons")
public class Seasons {
    @PrimaryKey
    private final String url;

    private final int year;

    public Seasons(int year, String url) {
        this.year = year;
        this.url = url;
    }

    public int getYear() {
        return year;
    }

    public String getUrl() {
        return url;
    }

    @Override
    public String toString() {
        return "Seasons{" +
                "url='" + url + '\'' +
                ", year=" + year +
                '}';
    }
}
