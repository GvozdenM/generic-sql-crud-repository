package ModelsPuigPedro;

import Annotations.EntityAnnotation;
import Annotations.ForeignKey;
import Annotations.PrimaryKey;

@EntityAnnotation(tableName = "constructorResults")
public class ConstructorResults {
    @PrimaryKey
    private final int constructorResultsId;

    @ForeignKey(references = "races", referenceField = "raceId")
    private final int raceId;

    @ForeignKey(references = "constructors", referenceField = "constructorsId")
    private final int constructorId;
    private final float points;
    private final String status;

    public ConstructorResults(int constructorResultsId, int raceId, int constructorId, float points, String status) {
        this.constructorResultsId = constructorResultsId;
        this.raceId = raceId;
        this.constructorId = constructorId;
        this.points = points;
        this.status = status;
    }

    public int getConstructorResultsId() {
        return constructorResultsId;
    }

    public int getRaceId() {
        return raceId;
    }

    public int getConstructorId() {
        return constructorId;
    }

    public float getPoints() {
        return points;
    }

    public String getStatus() {
        return status;
    }
}
