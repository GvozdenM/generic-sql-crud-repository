package ModelsPuigPedro;

import Annotations.EntityAnnotation;
import Annotations.PrimaryKey;

import java.sql.Date;

@EntityAnnotation(tableName = "drivers")
public class Drivers {
    @PrimaryKey
    private final int driverId;
    private final String driverRef;
    private final int number;
    private final String code;
    private final String forename;
    private final String surname;
    private final Date dob;
    private final String nationality;
    private final String url;

    public Drivers(int driverId,
                   String driverRef,
                   int number,
                   String code,
                   String forename,
                   String surname,
                   Date dob,
                   String nationality,
                   String url) {
        this.driverId = driverId;
        this.driverRef = driverRef;
        this.number = number;
        this.code = code;
        this.forename = forename;
        this.surname = surname;
        this.dob = dob;
        this.nationality = nationality;
        this.url = url;
    }

    public int getDriverId() {
        return driverId;
    }

    public String getDriverRef() {
        return driverRef;
    }

    public int getNumber() {
        return number;
    }

    public String getCode() {
        return code;
    }

    public String getForename() {
        return forename;
    }

    public String getSurname() {
        return surname;
    }

    public Date getDob() {
        return dob;
    }

    public String getNationality() {
        return nationality;
    }

    public String getUrl() {
        return url;
    }
}
