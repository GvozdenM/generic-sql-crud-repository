package ModelsPuigPedro;

import Annotations.EntityAnnotation;
import Annotations.ForeignKey;
import Annotations.PrimaryKey;

@EntityAnnotation(tableName = "qualifying")
public class Qualifying {
    @PrimaryKey
    private final int qualifyId;

    @ForeignKey(references = "races", referenceField = "raceId")
    private final int raceId;

    @ForeignKey(references = "drivers", referenceField = "driverId")
    private final int driverId;

    @ForeignKey(references = "constructors", referenceField = "constructorsId")
    private final int constructorId;
    private final int number;
    private final int position;
    private final String q1;
    private final String q2;
    private final String q3;

    public Qualifying(int qualifyId,
                      int raceId,
                      int driverId,
                      int constructorId,
                      int number,
                      int position,
                      String q1,
                      String q2,
                      String q3) {
        this.qualifyId = qualifyId;
        this.raceId = raceId;
        this.driverId = driverId;
        this.constructorId = constructorId;
        this.number = number;
        this.position = position;
        this.q1 = q1;
        this.q2 = q2;
        this.q3 = q3;
    }

    public int getQualifyId() {
        return qualifyId;
    }

    public int getRaceId() {
        return raceId;
    }

    public int getDriverId() {
        return driverId;
    }

    public int getConstructorId() {
        return constructorId;
    }

    public int getNumber() {
        return number;
    }

    public int getPosition() {
        return position;
    }

    public String getQ1() {
        return q1;
    }

    public String getQ2() {
        return q2;
    }

    public String getQ3() {
        return q3;
    }
}
