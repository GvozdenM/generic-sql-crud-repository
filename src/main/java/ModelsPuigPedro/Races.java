package ModelsPuigPedro;

import Annotations.EntityAnnotation;
import Annotations.PrimaryKey;

import java.sql.Date;
import java.sql.Time;

@EntityAnnotation(tableName = "races")
public class Races {
    @PrimaryKey
    private final int raceId;
    private final int year;
    private final int round;
    private final int circuitId;
    private final String name;
    private final Date date;
    private final Time time;
    private final String url;

    public Races(int raceId,
                 int year,
                 int round,
                 int circuitId,
                 String name,
                 Date date,
                 Time time,
                 String url) {

        this.raceId = raceId;
        this.year = year;
        this.round = round;
        this.circuitId = circuitId;
        this.name = name;
        this.date = date;
        this.time = time;
        this.url = url;
    }

    public int getRaceId() {
        return raceId;
    }

    public int getYear() {
        return year;
    }

    public int getRound() {
        return round;
    }

    public int getCircuitId() {
        return circuitId;
    }

    public String getName() {
        return name;
    }

    public Date getDate() {
        return date;
    }

    public Time getTime() {
        return time;
    }

    public String getUrl() {
        return url;
    }
}
