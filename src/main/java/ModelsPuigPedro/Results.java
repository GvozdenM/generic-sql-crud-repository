package ModelsPuigPedro;

import Annotations.EntityAnnotation;
import Annotations.ForeignKey;
import Annotations.PrimaryKey;

@EntityAnnotation(tableName = "results")
public class Results {

    @PrimaryKey
    private final int resultId;

    @ForeignKey(references = "races", referenceField = "raceId")
    private final int raceId;

    @ForeignKey(references = "drivers", referenceField = "driverId")
    private final int driverId;

    @ForeignKey(references = "constructors", referenceField = "constructorsId")
    private final int constructorId;
    private final int number;
    private final int grid;
    private final int position;
    private final String positionText;
    private final int positionOrder;
    private final float points;
    private final int laps;
    private final String time;
    private final int milliseconds;
    private final int fastestLap;
    private final int rank;
    private final String fastestLapTime;
    private final String fastestLapSpeed;
    private final int statusId;

    public Results(int resultId,
                   int raceId,
                   int driverId,
                   int constructorId,
                   int number,
                   int grid,
                   int position,
                   String positionText,
                   int positionOrder,
                   float points,
                   int laps,
                   String time,
                   int milliseconds,
                   int fastestLap,
                   int rank,
                   String fastestLapTime,
                   String fastestLapSpeed,
                   int statusId) {

        this.resultId = resultId;
        this.raceId = raceId;
        this.driverId = driverId;
        this.constructorId = constructorId;
        this.number = number;
        this.grid = grid;
        this.position = position;
        this.positionText = positionText;
        this.positionOrder = positionOrder;
        this.points = points;
        this.laps = laps;
        this.time = time;
        this.milliseconds = milliseconds;
        this.fastestLap = fastestLap;
        this.rank = rank;
        this.fastestLapTime = fastestLapTime;
        this.fastestLapSpeed = fastestLapSpeed;
        this.statusId = statusId;
    }

    public int getResultId() {
        return resultId;
    }

    public int getRaceId() {
        return raceId;
    }

    public int getDriverId() {
        return driverId;
    }

    public int getConstructorId() {
        return constructorId;
    }

    public int getNumber() {
        return number;
    }

    public int getGrid() {
        return grid;
    }

    public int getPosition() {
        return position;
    }

    public String getPositionText() {
        return positionText;
    }

    public int getPositionOrder() {
        return positionOrder;
    }

    public float getPoints() {
        return points;
    }

    public int getLaps() {
        return laps;
    }

    public String getTime() {
        return time;
    }

    public int getMilliseconds() {
        return milliseconds;
    }

    public int getFastestLap() {
        return fastestLap;
    }

    public int getRank() {
        return rank;
    }

    public String getFastestLapTime() {
        return fastestLapTime;
    }

    public String getFastestLapSpeed() {
        return fastestLapSpeed;
    }

    public int getStatusId() {
        return statusId;
    }
}
