package ModelsPuigPedro;

import Annotations.EntityAnnotation;
import Annotations.PrimaryKey;

@EntityAnnotation(tableName = "status")
public class Status {
    @PrimaryKey
    private final int statusId;
    private final String status;

    public Status(int statusId, String status) {
        this.statusId = statusId;
        this.status = status;
    }

    public int getStatusId() {
        return statusId;
    }

    public String getStatus() {
        return status;
    }

    @Override
    public String toString() {
        return "Status{" +
                "statusId=" + statusId +
                ", status='" + status + '\'' +
                '}';
    }
}
