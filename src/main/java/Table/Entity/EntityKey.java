package Table.Entity;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;

public abstract class EntityKey {
    private final String sqlType;
    private final boolean isPrimaryKey;
    private final Field keyClassField;

    public EntityKey(@NotNull String sqlType,
                     boolean isPrimaryKey,
                     @NotNull Field keyClassField) {
        this.sqlType = sqlType;
        this.isPrimaryKey = isPrimaryKey;
        this.keyClassField = keyClassField;
    }

    public String getSqlType() {
        return sqlType;
    }

    public boolean isPrimaryKey() {
        return isPrimaryKey;
    }

    public Field getKeyClassField() {
        return keyClassField;
    }

    public String getKeyClassFiledName() {
        return this.keyClassField.getName();
    }

    public String getColumnName() {
        return this.columnName(this.keyClassField.getName());
    }

    public abstract String getKeyConstraint();

    protected String columnName(String s) {
        return s;
    }
}
