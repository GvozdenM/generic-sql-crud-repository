package Table.Entity;

import Annotations.EntityAnnotation;
import Annotations.ForeignKey;
import Annotations.PrimaryKey;
import Table.EntityMapping.SqlTypeTransform;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

/**
 * Class provides static helper methods to create {@link Table} abstraction and to use
 * its properties to retrieve base statements.
 */
public class TableManager {
    /**
     * Create a table identified by the given name and which is modeled on the class passed
     * @param entityClass The class to model the table on
     * @param <V> Generic representing the type of class
     *
     * @return A table modeled on the class definition passed
     */
    public static <V> Table<V> createTable(@NotNull final Class<V> entityClass) {
        Map<String, Field> mappableFields = getMappableClassFields(entityClass);
        String tableName = getTableName(entityClass);
        EntityKey primaryKeyField = getPrimaryKeyField(entityClass);
        Collection<EntityKey> foreignKeys = foreignKeys(entityClass);

        //create the table
        return new Table<>(mappableFields, entityClass, tableName, foreignKeys, primaryKeyField);
    }

    private static <V> String getTableName(@NotNull final Class<V> entityClass) {
        //get this class's entity annotations
        EntityAnnotation entityAnnotation = entityClass.getAnnotation(EntityAnnotation.class);

        //if there are no annotations of this type its not an entity so throw error
        if(null == entityAnnotation) {
            throw new RuntimeException("Class has not been annotated with @EntityAnnotation");
        }

        //get the name from the annotation
        return entityAnnotation.tableName();
    }

    /**
     * Get the foreign keys as defined by the annotations on the given Class object
     * @param <V> The type of the object whose type definition is defined by the class object passed.
     * @param entityClass The class object from which to read the persistence properites
     * @return The foreign keys
     */
    private static <V> Collection<EntityKey> foreignKeys(@NotNull Class<V> entityClass) {
        ArrayList<EntityKey> keys = new ArrayList<>();
        String type = null;

        for(Field field : entityClass.getDeclaredFields()) {
            //get fk annotation
            ForeignKey fkAnnotation = field.getAnnotation(ForeignKey.class);

            //there was an annotation attached to the field so this is a foreign key
            if(null != fkAnnotation) {

                //get the sql type for this key (probably integer)
                type = !fkAnnotation.type().equals("") ?
                        fkAnnotation.type() :
                        SqlTypeTransform.getFieldSqlType(field.getGenericType());

                //add a new key entry
                keys.add(new ForeignEntityKey(
                        type,
                        false,
                        field,
                        fkAnnotation.references(),
                        fkAnnotation.referenceField()
                ));
            }
        }

        return keys;
    }

    /**
     * Return the field of of the class entityClass which is annotated with {@link PrimaryKey}
     * @param <V> Type of class
     * @param entityClass The class to find the primary key off
     * @return The primary key field
     */
    private static <V> EntityKey getPrimaryKeyField(@NotNull final Class<V> entityClass) {
        Field primaryKey = null;

        for(Field field : entityClass.getDeclaredFields()) {
            PrimaryKey primaryKeyAnnotation = field.getAnnotation(PrimaryKey.class);

            if(null != primaryKeyAnnotation) {
                primaryKey = field;
                break;
            }
        }

        if(null == primaryKey) {
            throw new RuntimeException("Primary key field not defined!");
        }

        return new PrimaryEntityKey(
                SqlTypeTransform.getFieldSqlType(primaryKey.getGenericType()),
                true,
                primaryKey
        );
    }

    /**
     * Function to retrieve all non-transient and non-synthetic fields of a class.
     * @param entityClass The class to retrieve the fields from
     *
     * @return The fields mappable (non-transient and non-synthetic) fields of the class
     */
    private static Map<String, Field> getMappableClassFields(@NotNull final Class<?> entityClass) {
        return new ArrayList<>(List.of(entityClass.getDeclaredFields()))
                .stream()
                .filter((aField) -> !aField.isSynthetic())
                .filter((aField) -> !(Modifier.isTransient(aField.getModifiers())))
                .collect(Collectors.toMap(Field::getName, Function.identity()));
    }
}
