package Table.Entity;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * Table abstraction class which provides type description for a table generated based
 * on the object representation of a particular entity.
 *
 */
public class Table<V> {
    private final Class<V> entityClass;
    private final Map<String, Field> columns;
    private final String tableName;
    private final Collection<EntityKey> keys;
    private final EntityKey primaryKey;

    /**
     * Basic constructor for table which takes the table name from {@link Class#getName()}
     * @param columns Column names for this table
     * @param entityClass The entity class which this table maps to
     * @param keys Collection of key entries for this table
     * @param primaryKey Primary key for this table
     */
    public Table(@NotNull Map<String, Field> columns,
                 @NotNull Class<V> entityClass,
                 @NotNull Collection<EntityKey> keys,
                 @NotNull EntityKey primaryKey) {
        this(columns, entityClass, entityClass.getName(), keys, primaryKey);
    }

    /**
     * Basic constructor for table which takes the table name the parameter passed.
     *  @param columns Column names for this table
     * @param entityClass The entity class which this table maps to
     * @param keys Collection of key entries for this table
     * @param primaryKey Primary key for this table
     */
    public Table(@NotNull Map<String, Field> columns,
                 @NotNull Class<V> entityClass,
                 @NotNull String tableName,
                 @NotNull Collection<EntityKey> keys,
                 @NotNull EntityKey primaryKey)
    {
        this.columns = new LinkedHashMap<>(columns);
        this.entityClass = entityClass;
        this.tableName = tableName;
        this.keys = keys;
        this.primaryKey = primaryKey;
    }

    public Collection<String> getColumnNames() {
        return this.columns.keySet();
    }

    public Collection<Field> getColumnFields() {
        return this.columns.values();
    }

    public Class<V> getEntityClass() {
        return this.entityClass;
    }

    public String getTableName(){
        return this.tableName;
    }

    public String getPrimaryKeyFieldName() {
        return this.primaryKey.getColumnName();
    }

    public Field getFieldByName(@NotNull String name) {
        return this.columns.get(name);
    }

    public Collection<EntityKey> getForeignKeys() {
        return this.keys;
    }

    public EntityKey getPrimaryKey() {
        return this.primaryKey;
    }

    @Override
    public String toString() {
        return "Table{" +
                "entityClass=" + entityClass +
                ", columns=" + columns +
                ", tableName='" + tableName + '\'' +
                ", keys=" + keys +
                ", primaryKey=" + primaryKey +
                '}';
    }
}
