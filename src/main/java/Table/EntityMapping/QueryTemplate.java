package Table.EntityMapping;

import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.regex.MatchResult;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class QueryTemplate {
    public static final String TEMPLATE_GET_BY_ID = "SELECT * FROM :tableName AS t WHERE t.:pkField = :pkValue ";
    public static final String TEMPLATE_GET_ALL = "SELECT * FROM :tableName ";
    public static final String TEMPLATE_INSERT = "INSERT INTO :tableName ( :columnNames ) VALUES ( :values )";
    public static final String TEMPLATE_COUNT = "SELECT Count(*) AS count FROM :tableName ";
    public static final String TEMPLATE_CREATE = "CREATE TABLE :tableName ( :columnDefinitions , :constraints )";
    public static final String TEMPLATE_COLUMN_DEFINITION = ":name :type ";
    public static final String TEMPLATE_DELETE = "DELETE FROM :tableName AS t WHERE t.:pkField = :pkValue ";

    private static final Pattern PATTERN_VARIABLE = Pattern.compile("(?<=:)(.*?)(?= )");

    private final String template;

    public QueryTemplate(@NotNull String template) {
        this.template = template;
    }

    public List<String> getVariables() {
        Matcher matcher = PATTERN_VARIABLE.matcher(this.template);
        return matcher
                .results()
                .map(MatchResult::group)
                .collect(Collectors.toList());
    }

    public String getTemplate() {
        return this.template;
    }
}
