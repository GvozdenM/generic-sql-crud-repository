package Table.EntityMapping.Exceptions;

public class InvalidQueryTemplateVariableException extends RuntimeException {
    public InvalidQueryTemplateVariableException(String s) {
        super(s);
    }
}
