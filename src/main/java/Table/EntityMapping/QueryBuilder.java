package Table.EntityMapping;

import Table.EntityMapping.Exceptions.InvalidQueryTemplateVariableException;
import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.Map;

public class QueryBuilder {
    private QueryTemplate template;
    private final Map<String, String> variables = new LinkedHashMap<>();

    public QueryBuilder() {
        this.template = null;
    }

    public QueryBuilder(@NotNull String template) {
        this.template = new QueryTemplate(template);
    }

    public <V> QueryBuilder addParameter(@NotNull String key, @NotNull V value) {
        if(!template.getVariables().contains(key)) {
            throw new InvalidQueryTemplateVariableException("Variable " + key + " not in template");
        }

//        if(value instanceof java.lang.String) {
//            this.variables.put(key, "'" + value + "'");
//        }
//        else {
//            this.variables.put(key, value.toString());
//        }

        this.variables.put(key, value.toString());

        return this;
    }

    public String buildQuery() {
        String templateString = this.template.getTemplate();

        for(String varName : this.variables.keySet()) {
            templateString = this.substituteParameter(templateString, varName);
        }

        return templateString.trim() + ";";
    }

    private String substituteParameter(@NotNull String queryTemplate, @NotNull String variableName) {
        return queryTemplate.replaceFirst(":" + variableName, this.variables.get(variableName));
    }

    private void removeInvalidVariables() {
        //get valid var collection
        Collection<String> templateVars = this.template.getVariables();

        for(String var : this.variables.keySet()) {
            //remove var if not valid for this template
            if(!templateVars.contains(var)) {
                this.variables.remove(var);
            }
        }
    }

    public QueryBuilder setTemplate(@NotNull QueryTemplate template) {
        this.template = template;
        this.removeInvalidVariables();

        return this;
    }

    public Collection<String> getAssignedVars() {
        return this.variables.keySet();
    }

    public QueryBuilder clear() {
        this.variables.clear();

        return this;
    }
}
