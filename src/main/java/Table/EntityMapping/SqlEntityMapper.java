package Table.EntityMapping;

import Table.Entity.EntityKey;
import Table.Entity.Table;
import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.sql.Date;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

public class SqlEntityMapper implements EntityMapper {

    private final QueryBuilder queryBuilder;

    public SqlEntityMapper(QueryBuilder queryBuilder) {
        this.queryBuilder = queryBuilder;
    }

    @Override
    public <T> String insertQueryStatement(@NotNull Table<T> table, @NotNull T object) {
        String values = this.entityValues(table, object);
        String columns = this.tableColumns(table);

        return this.queryBuilder
                        .clear()
                        .setTemplate(new QueryTemplate(QueryTemplate.TEMPLATE_INSERT))
                        .addParameter("tableName", table.getTableName())
                        .addParameter("columnNames", columns)
                        .addParameter("values", values)
                        .buildQuery();
    }

    @Override
    public <T> String getByIdQueryStatement(@NotNull Table<T> table, @NotNull Long id) {
        return this.queryBuilder
                .clear()
                .setTemplate(new QueryTemplate(QueryTemplate.TEMPLATE_GET_BY_ID))
                .addParameter("tableName", table.getTableName())
                .addParameter("pkField", table.getPrimaryKeyFieldName())
                .addParameter("pkValue", id)
                .buildQuery();
    }

    @Override
    public <T> String getAllQueryStatement(@NotNull Table<T> table) {
        return this.queryBuilder
                .clear()
                .setTemplate(new QueryTemplate(QueryTemplate.TEMPLATE_GET_ALL))
                .addParameter("tableName", table.getTableName())
                .buildQuery();
    }

    @Override
    public <T> String countQueryStatement(@NotNull Table<T> table) {
        return this.queryBuilder
                .clear()
                .setTemplate(new QueryTemplate(QueryTemplate.TEMPLATE_COUNT))
                .addParameter("tableName", table.getTableName())
                .buildQuery();
    }

    @Override
    public <T> String createQueryStatement(@NotNull Table<T> table) {
        String constraints = this.getKeyConstraintStatement(table);
        String columns = this.columnDefinitions(table);

        return this.queryBuilder
                .clear()
                .setTemplate(new QueryTemplate(QueryTemplate.TEMPLATE_CREATE))
                .addParameter("tableName", table.getTableName())
                .addParameter("columnDefinitions", columns)
                .addParameter("constraints", constraints)
                .buildQuery();
    }

    @Override
    public <T, K> String deleteQueryStatement(@NotNull Table<T> table, @NotNull K id) {
        return this.queryBuilder
                .clear()
                .setTemplate(new QueryTemplate(QueryTemplate.TEMPLATE_DELETE))
                .addParameter("tableName", table.getTableName())
                .addParameter("pkField", table.getPrimaryKeyFieldName())
                .addParameter("pkValue", id.toString())
                .buildQuery();
    }

    private <T> String getKeyConstraintStatement(@NotNull Table<T> table) {
        Collection<EntityKey> foreignKeys = table.getForeignKeys();
        String[] constraints = new String[foreignKeys.size() + 1];

        constraints[0] = table.getPrimaryKey().getKeyConstraint();

        int i = 1;
        for(EntityKey entityKey : foreignKeys) {
            constraints[i] = entityKey.getKeyConstraint();

            i++;
        }

        return String.join(", ", constraints);
    }

    private <T> String tableColumns(@NotNull Table<T> table) {
        ArrayList<String> columnNames = new ArrayList<>();

        for(String column : table.getColumnNames()) {
            columnNames.add(columnName(column));
        }

        return String.join(", ", columnNames);
    }

    private <T> String columnDefinitions(@NotNull Table<T> table) {
        Collection<Field> columns = table.getColumnFields();
        ArrayList<String> columnDefinitions = new ArrayList<>();
        for(Field field : columns) {
            columnDefinitions.add(SqlTypeTransform.getSqlFieldDefinition(this.columnName(field.getName()), field));
        }

        return String.join(", ", columnDefinitions);
    }

    private String columnName(String s) {
        return s;
    }

    private <T> String entityValues(@NotNull Table<T> table, @NotNull T object) {
        String[] template = new String[table.getColumnNames().size()];

        try {
            int i = 0;
            for (Field field : table.getColumnFields()) {

                field.setAccessible(true);
//                    template[i] = null == field.get(object) ? null : field.get(object).toString();

                Object data = field.get(object);
                if (null != data) {
                    if (data instanceof String) {
                        template[i] = "'" + ((String) data).replaceAll("'", "''") + "'";
                    } else {

                        if(data instanceof java.sql.Date || data instanceof java.sql.Time) {
                            template[i] = "'" + data.toString() + "'";
                        }
                        else {
                            template[i] = data.toString();
                        }
                    }
                } else {
                    template[i] = null;
                }

                field.setAccessible(false);

                i++;
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return String.join(", ", template);
    }
}
