package Table.EntityMapping;

import Table.Entity.Table;
import org.jetbrains.annotations.NotNull;

public interface EntityMapper {
    <T> String insertQueryStatement(@NotNull Table<T> table, @NotNull T object);

    <T> String getByIdQueryStatement(@NotNull Table<T> table, @NotNull Long id);

    <T> String getAllQueryStatement(@NotNull Table<T> table);

    <T> String countQueryStatement(@NotNull Table<T> table);

    <T> String createQueryStatement(@NotNull Table<T> testTable);

    <T, K> String deleteQueryStatement(@NotNull Table<T> testTable, @NotNull K id);
}
