package Table.EntityMapping;

import org.jetbrains.annotations.NotNull;

import java.lang.reflect.Field;
import java.lang.reflect.Type;

public interface SqlTypeTransform {
    static String getSqlFieldDefinition(@NotNull String name, @NotNull Field aField) {
        Type type = aField.getGenericType();
        return name + " " + getFieldSqlType(type);
    }

    static String getFieldSqlType(Type type) {
        String definition = "";
        switch (type.getTypeName()) {
            case "java.lang.String":
                definition = definition + "Text";
                break;
            case "int":
                definition = definition + "integer";
                break;
            case "long":
                definition = definition + "bigint";
                break;
            case "float":
                definition = definition + "real";
                break;
            case "double":
                definition = definition + "double precision";
                break;
            case "boolean":
                definition = definition + "bit";
                break;
            case "byte":
                definition = definition + "tinyint";
                break;
            case "java.sql.Time":
                definition = definition + "bigtime";
                break;
            case "java.sql.Timestamp":
                definition = definition + "bigdatetime";
                break;
            case "java.sql.Date":
                definition = definition + "date";
        }

        return definition;
    }
}
