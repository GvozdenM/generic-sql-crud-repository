package Repositories;

import org.jetbrains.annotations.NotNull;

import java.util.Collection;
import java.util.Optional;

/**
 * Generic repository for database entities
 * @param <T> The type of entity this repository handles
 */
public interface CrudRepository<T> {
    /**
     * Return all persisted entities of the type managed
     * by this repository
     *
     * @return A {@link Collection<T>} of entities of the type managed by
     * this repository a zero length collection if no entities of this type are persisted
     */
    Collection<T> getAll();

    /**
     * Return an entity from the persistence mechanism which is
     * identified by the id provided
     *
     * @param id The id of the entity to get
     * @return An {@link Optional<T>} containing an entity with the provided id or
     * an empty {@link Optional<T>} if no entity with that id is persisted
     */
    Optional<T> get(@NotNull Long id);

    /**
     * Create a new entry in the persistence mechanism for the entity
     * passed.
     *
     * @param entity The entity to persist
     * @return An {@link Optional<T>} containing the entity, updated with a new
     * identifier if it has been persisted successfully or an empty {@link Optional<T>}
     */
    Optional<T> create(@NotNull T entity);

    /**
     * Update an existing entity who's id matches the given entity or
     * create a new entity if no id is assigned.
     *
     * @param entity The entity to update or persist
     * @return An {@link Optional<T>} containing the entity with an updated identifier
     * if newly persisted or the new state of the entity in the database if update or
     * an empty {@link Optional<T>} if updating failed.
     */
    Optional<T> update(@NotNull T entity);

    <K> void delete(@NotNull K key);

    /**
     * Creates all the entities in the collection passed from the persistence
     * mechanism
     * @param entities The entities to create
     */
    Collection<T> batchCreate(Collection<T> entities);

    int countAll();
}
