package Repositories;

import Table.Entity.Table;
import Table.EntityMapping.EntityMapper;
import Table.EntityMapping.QueryBuilder;
import Table.EntityMapping.SqlEntityMapper;
import Transformers.ResultSetTransformer;
import org.jetbrains.annotations.NotNull;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.*;

public class AbstractJDBCRepository<T> implements CrudRepository<T> {
    private final Connection connection;
    private final String sourceDatabase;
    private final Table<T> table;
    private final ResultSetTransformer<T> transformer;
    private final EntityMapper em = new SqlEntityMapper(new QueryBuilder());

    public AbstractJDBCRepository(@NotNull Connection connection,
                                  @NotNull String sourceDatabase,
                                  @NotNull Table<T> table,
                                  @NotNull ResultSetTransformer<T> transformer) {
        this.connection = connection;
        this.sourceDatabase = sourceDatabase;
        this.table = table;
        this.transformer = transformer;

        try {
            connection.createStatement().executeQuery("USE " + sourceDatabase);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    public Class<T> getManagedClass() {
        return this.table.getEntityClass();
    }

    protected final ResultSet executeQuery(String query) throws SQLException {
        return this.connection.createStatement().executeQuery(query);
    }

    protected final int executeUpdate(String query) throws SQLException {
        return this.connection.createStatement().executeUpdate(query);
    }

    public void createTable() {
        final String query = this.em.createQueryStatement(this.table);
        try {
            this.executeQuery(query);
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Return all persisted entities of the type managed
     * by this repository
     *
     * @return A {@link Collection <T>} of entities of the type managed by
     * this repository a zero length collection if no entities of this type are persisted
     */
    public Collection<T> getAll() {
        final String query = this.em.getAllQueryStatement(this.table);
        return queryResultList(query);
    }

    /**
     * Return an entity from the persistence mechanism which is
     * identified by the id provided
     *
     * @param id The id of the entity to get
     * @return An {@link Optional <T>} containing an entity with the provided id or
     * an empty {@link Optional<T>} if no entity with that id is persisted
     */
    public Optional<T> get(@NotNull Long id) {
        String query = this.em.getByIdQueryStatement(this.table, id);
        T result = queryResult(query);

        return Optional.ofNullable(result);
    }
    /**
     * Create a new entry in the persistence mechanism for the entity
     * passed.
     *
     * @param entity The entity to persist
     * @return An {@link Optional<T>} containing the entity, updated with a new
     * identifier if it has been persisted successfully or an empty {@link Optional<T>}
     */

    public Optional<T> create(@NotNull T entity){
        String query = this.em.insertQueryStatement(this.table, entity);

        try {
            executeUpdate(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

        return Optional.ofNullable(entity);
    }

    public <K> void delete(@NotNull K key) {
        String query = this.em.deleteQueryStatement(this.table, key);
        try {
            this.executeUpdate(query);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    public Collection<T> batchCreate(Collection<T> entities) {
        List<T> persisted = new ArrayList<>();

        for(T entity : entities) {
            String query = this.em.insertQueryStatement(this.table, entity);
            persisted.add(this.queryResult(query));
        }

        return persisted;
    }

    @Override
    public int countAll() {
        String query = this.em.countQueryStatement(this.table);
        try {
            ResultSet resultSet = this.executeQuery(query);
            resultSet.next();

            return resultSet.getInt("count");
        } catch (SQLException throwables) {
            throwables.printStackTrace();
            return 0;
        }
    }

    /**
     * Update an existing entity who's id matches the given entity or
     * create a new entity if no id is assigned.
     *
     * @param entity The entity to update or persist
     * @return An {@link Optional<T>} containing the entity with an updated identifier
     * if newly persisted or the new state of the entity in the database if update or
     * an empty {@link Optional<T>} if updating failed.
     */
    public Optional<T> update(@NotNull T entity){
        throw new RuntimeException("UNIMPLEMENTED UPDATE");
    }

    /**
     * get the result set according to the query being passed
     * @param query query that is going to be executed
     * @return The result of type T.
     */
    private T queryResult(String query) {
        ResultSet resultSet;
        T result = null;
        try {
            resultSet = this.executeQuery(query);
            result = this.transformer.transform(resultSet);
        }
        catch (SQLException e){
            System.out.println("Not connected to database!");
            System.out.println(e.getMessage());
        }
        return result;
    }

    private List<T> queryResultList(String query) {
        ResultSet resultSet;
        List<T> result = new ArrayList<>();
        try {
            resultSet = this.executeQuery(query);

            while(resultSet.next()) {
                result.add(this.transformer.transform(resultSet));
            }
        }
        catch (SQLException e){
            System.out.println("Not connected to database!");
            System.out.println(e.getMessage());
        }
        return result;
    }
}


