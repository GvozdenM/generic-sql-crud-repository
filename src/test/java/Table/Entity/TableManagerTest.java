package Table.Entity;

import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TableManagerTest {
    @Test
    public void createsTableCorrectly() {
        Class<TestEntity> testEntityClass = TestEntity.class;
        Table<TestEntity> table = TableManager.createTable(testEntityClass);
        String[] expectedColumns = {"pkInt", "name"};

        for(String column : expectedColumns) {
            assertTrue(table.getColumnNames().contains(column));
        }

        assertEquals(expectedColumns.length, table.getColumnNames().size());
        assertEquals(TestEntity.class, table.getEntityClass());
        assertEquals( "TestEntity", table.getTableName());
        assertEquals("pkInt", table.getPrimaryKeyFieldName());
    }
}
