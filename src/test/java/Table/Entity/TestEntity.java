package Table.Entity;

import Annotations.EntityAnnotation;
import Annotations.PrimaryKey;

@EntityAnnotation(tableName = "TestEntity")
public class TestEntity {
    @PrimaryKey
    private final int pkInt;
    private final String name;

    public TestEntity(int pkInt, String name) {
        this.pkInt = pkInt;
        this.name = name;
    }

    public int getPkInt() {
        return pkInt;
    }

    public String getName() {
        return name;
    }
}
