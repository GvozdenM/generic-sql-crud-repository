package Table.Entity;

import Table.EntityMapping.EntityMapper;
import Table.EntityMapping.QueryBuilder;
import Table.EntityMapping.SqlEntityMapper;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class EntityMapperTest {
    private static Table<TestEntity> testTable;
    private static EntityMapper entityMapper;

    @BeforeAll
    static void setUpTable() {
        testTable = TableManager.createTable(TestEntity.class);
    }

    @BeforeAll
    static void setupEntityMapper() {
        entityMapper = new SqlEntityMapper(new QueryBuilder());
    }

    @Test
    public void createsProperEntityByIdQuery() {
        String expectedQuery = "SELECT * FROM TestEntity AS t WHERE t.pkInt = 1;";

        assertEquals(expectedQuery, entityMapper.getByIdQueryStatement(testTable, 1L));
    }

    @Test
    public void createsProperGetAllQuery() {
        String expectedQuery = "SELECT * FROM TestEntity;";

        assertEquals(expectedQuery, entityMapper.getAllQueryStatement(testTable));
    }

    @Test
    public void createsProperCountQuery() {
        String expectedQuery = "SELECT Count(*) FROM TestEntity;";

        assertEquals(expectedQuery, entityMapper.countQueryStatement(testTable));
    }

    @Test
    public void createsProperCreateStatement() {
        String expectedQuery = "CREATE TABLE testEntity2 ( id integer, fk_id integer , PRIMARY KEY (id), FOREIGN KEY(fk_id) REFERENCES TestEntity(id) );";        Table<TestEntityWithFks> lTestTable = TableManager.createTable(TestEntityWithFks.class);

        assertEquals(expectedQuery, entityMapper.createQueryStatement(lTestTable));
    }

    @Test
    public void createsInsertStatementProperly() {
        String expectedQuery = "INSERT INTO TestEntity ( pk_int, name ) VALUES ( 1, 11 );";

        assertEquals(expectedQuery, entityMapper.insertQueryStatement(testTable, new TestEntity(1, "11")));
    }

    @Test
    public void deleteQueryStatemementMadeProperly() {
        String expectedQuery = "DELETE FROM TestEntity AS t WHERE t.pk_int = 15;";

        assertEquals(expectedQuery, entityMapper.deleteQueryStatement(testTable, 15));
    }
}
