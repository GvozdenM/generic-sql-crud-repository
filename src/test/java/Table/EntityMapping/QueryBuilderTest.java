package Table.EntityMapping;

import Table.EntityMapping.Exceptions.InvalidQueryTemplateVariableException;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class QueryBuilderTest {
    @Test
    public void testQueryBuilderStoresVariablesCorrectly() {
        String template = "SELECT * FROM :tableName AS t WHERE t.id = :id ";
        List<String> correctVariables = List.of("tableName", "id");
        List<String> faultyVariables = List.of("tableName", "id", "idField");

        QueryBuilder queryBuilder = new QueryBuilder(template);

        for(String var : correctVariables) {
            assertDoesNotThrow(() -> queryBuilder.addParameter(var, var + "added"));
        }


        assertThrows(
                InvalidQueryTemplateVariableException.class,
                () -> queryBuilder.addParameter("idField", "idField" + "added")
        );
    }

    @Test
    public void testQueryBuilderFillsTemplateCorrectly() {
        String template = "SELECT * FROM :tableName AS t WHERE t.id = :id AND t.idUser = :idUser ";
        String expected = "SELECT * FROM testTable AS t WHERE t.id = 11 AND t.idUser = 15;";
        QueryBuilder queryBuilder = new QueryBuilder(template);

        //assign variables
        queryBuilder.addParameter("tableName", "testTable");
        queryBuilder.addParameter("id", "11");
        queryBuilder.addParameter("idUser", "15");

        assertEquals(expected, queryBuilder.buildQuery());
    }

    @Test
    public void testQueryBuilderAddsSemicolonToEndOfQuery() {
        String template = "SELECT * FROM :tableName AS t WHERE t.id = :id ";
        QueryBuilder queryBuilder = new QueryBuilder(template);

        assertTrue(queryBuilder.buildQuery().endsWith(";"));
    }

    @Test
    public void testQueryBuilderRemovesInvalidVariablesOnTemplateSwap() {
        String template = "SELECT * FROM :tableName AS t WHERE t.id = :id ";
        QueryBuilder queryBuilder = new QueryBuilder(template);
        String templateModified = "SELECT * FROM :tableName AS t ";

        //assign variables
        queryBuilder.addParameter("tableName", "testTable");
        queryBuilder.addParameter("id", "id");

        //assert assigned vars are assigned
        assertTrue(queryBuilder.getAssignedVars().contains("tableName"));
        assertTrue(queryBuilder.getAssignedVars().contains("id"));

        //swap template
        queryBuilder.setTemplate(new QueryTemplate(templateModified));

        //assert key field which is no longer in template has been unassigned
        assertTrue(queryBuilder.getAssignedVars().contains("tableName"));
        assertFalse(queryBuilder.getAssignedVars().contains("id"));
    }
}