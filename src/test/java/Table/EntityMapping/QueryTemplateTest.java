package Table.EntityMapping;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class QueryTemplateTest {
    @Test
    public void createTableTemplateContainsCorrectVariables() {
        final String queryTemplateString = QueryTemplate.TEMPLATE_CREATE;
        QueryTemplate queryTemplate = new QueryTemplate(queryTemplateString);
        List<String> templateVariables = queryTemplate.getVariables();
        List<String> expectedVariables = List.of("tableName", "columnDefinitions", "constraints");

        for(String templateVariable : templateVariables) {
            assertTrue(expectedVariables.contains(templateVariable));
        }

        assertEquals(expectedVariables.size(), templateVariables.size());
    }

    @Test
    public void insertIntoTableTemplateContainsCorrectVariables() {
        QueryTemplate queryTemplate = new QueryTemplate(QueryTemplate.TEMPLATE_INSERT);
        List<String> tempVars = queryTemplate.getVariables();
        List<String> expVars = List.of("tableName", "columnNames", "values");

        for(String templateVariable : tempVars) {
            assertTrue(expVars.contains(templateVariable));
        }

        assertEquals(expVars.size(), tempVars.size());
    }

    @Test
    public void getAllTemplateContainsCorrectVariables() {
        QueryTemplate queryTemplate = new QueryTemplate(QueryTemplate.TEMPLATE_GET_ALL);
        List<String> tempVars = queryTemplate.getVariables();
        List<String> expVars = List.of("tableName");

        for(String templateVariable : tempVars) {
            assertTrue(expVars.contains(templateVariable));
        }

        assertEquals(expVars.size(), tempVars.size());
    }

    @Test
    public void countAllTemplateContainsCorrectVariables() {
        QueryTemplate queryTemplate = new QueryTemplate(QueryTemplate.TEMPLATE_COUNT);
        List<String> tempVars = queryTemplate.getVariables();
        List<String> expVars = List.of("tableName");

        for(String templateVariable : tempVars) {
            assertTrue(expVars.contains(templateVariable));
        }

        assertEquals(expVars.size(), tempVars.size());
    }

    @Test
    public void getByIdTemplateContainsCorrectVariables() {
        QueryTemplate queryTemplate = new QueryTemplate(QueryTemplate.TEMPLATE_GET_BY_ID);
        List<String> tempVars = queryTemplate.getVariables();
        List<String> expVars = List.of("tableName", "pkField", "pkValue");

        for(String templateVariable : tempVars) {
            assertTrue(expVars.contains(templateVariable));
        }

        assertEquals(expVars.size(), tempVars.size());
    }

    @Test
    public void columnDefinitionTemplateContainsCorrectVariables() {
        QueryTemplate queryTemplate = new QueryTemplate(QueryTemplate.TEMPLATE_COLUMN_DEFINITION);
        List<String> tempVars = queryTemplate.getVariables();
        List<String> expVars = List.of("type", "name");

        for(String templateVariable : tempVars) {
            assertTrue(expVars.contains(templateVariable));
        }

        assertEquals(expVars.size(), tempVars.size());
    }
}